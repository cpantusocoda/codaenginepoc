<?php

namespace Agencycoda\Core\Controllers;


use Illuminate\Routing\Controller;
use Agencycoda\Core\Traits\ServiceableTrait;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    use ServiceableTrait;

    public function index(Request $request)
    {
        return $this->traitMethod();
    }
}
